@extends('layouts.default')
@section('content')
  <div class="mx-auto max-w-7xl px-4 sm:px-6 lg:px-8">
    <!-- We've used 3xl here, but feel free to try other max-widths based on your needs -->
    <div class="mx-auto max-w-3xl">

      <form class="space-y-6 bg-white" action="{{ route('ticket.store') }}" method="POST" enctype="multipart/form-data">
        @csrf
        <div class="bg-white px-4 py-5 shadow sm:rounded-lg sm:p-6">
          <div class="">
            <h3 class="text-lg font-medium leading-6 text-gray-900">Nouveau ticket</h3>
            <p class="mt-1 text-sm text-gray-500">Informations liées à la création de votre ticket</p>
          </div>

          <hr class="border-gray-200 my-5">

          <div class="mt-5 md:col-span-2 md:mt-0">
            <div class="grid grid-cols-6 gap-6">
              <div class="col-span-6 sm:col-span-3">
                <label for="type" class="block text-sm font-medium ">Type *</label>
                <select id="type"
                        name="type"
                        class="mt-1 block w-full rounded-md border-gray-300 py-2 pl-3 pr-10 text-base focus:border-indigo-500 focus:outline-none focus:ring-indigo-500 sm:text-sm">
                  <option selected>Demande d'amélioration</option>
                  <option>Bug</option>
                  <option>Question</option>
                </select>
              </div>

              <div class="col-span-6 sm:col-span-3">
                <label for="priority" class="block text-sm font-medium ">Priorité *</label>
                <select id="priority"
                        name="priority"
                        class="mt-1 block w-full rounded-md border-gray-300 py-2 pl-3 pr-10 text-base focus:border-indigo-500 focus:outline-none focus:ring-indigo-500 sm:text-sm">
                  <option selected>Faible</option>
                  <option>Moyen</option>
                  <option>Fort</option>
                </select>
              </div>

              <div class="col-span-6">
                <label for="title" class="block text-sm font-medium ">Titre *</label>
                <input type="text" name="title" id="title" autocomplete="title" required
                       class="bg-gray-50 mt-1 block w-full rounded-md border-gray-300 shadow-sm focus:border-indigo-500 focus:ring-indigo-500 sm:text-sm"></textarea>
              </div>

              <div class="col-span-6">
                <label for="description " class="block text-sm font-medium ">Description *</label>
                <p class="mt-2 text-sm text-gray-500">Veuillez décrire votre demande.</p>
                <textarea id="description"
                          name="description"
                          rows="3"
                          class="bg-gray-50 mt-1 block w-full rounded-md border-gray-300 shadow-sm focus:border-blue-500 focus:ring-blue-500 sm:text-sm"></textarea>
              </div>

              <div class="col-span-6">
                <label for="url" class="block text-sm font-medium ">Page concernée</label>
                <p class="mt-2 text-sm text-gray-500">Sur quelle page étiez-vous lorsque vous avez rencontré ce
                                                      problème ?</p>
                <input type="text" name="url" id="url" autocomplete="url"
                       class="bg-gray-50 mt-1 block w-full rounded-md border-gray-300 shadow-sm focus:border-indigo-500 focus:ring-indigo-500 sm:text-sm">
              </div>

              <div class="col-span-6" id="uploader">
                <label class="block text-sm font-medium ">Pièce jointe</label>
                <p class="mt-2 text-sm text-gray-500">Merci de nous faire parvenir des fichiers pouvant illustrer
                                                      votre demande ou votre idée</p>
                <div class="mt-1 flex justify-center rounded-md border-2 border-dashed border-gray-300 px-6 pt-5 pb-6">
                  <div class="space-y-1 text-center">
                    <svg class="mx-auto h-12 w-12 text-gray-400" stroke="currentColor" fill="none"
                         viewBox="0 0 48 48" aria-hidden="true">
                      <path
                        d="M28 8H12a4 4 0 00-4 4v20m32-12v8m0 0v8a4 4 0 01-4 4H12a4 4 0 01-4-4v-4m32-4l-3.172-3.172a4 4 0 00-5.656 0L28 28M8 32l9.172-9.172a4 4 0 015.656 0L28 28m0 0l4 4m4-24h8m-4-4v8m-12 4h.02"
                        stroke-width="2" stroke-linecap="round" stroke-linejoin="round" />
                    </svg>
                    <div class="flex text-sm text-gray-600">
                      <label for="file-upload"
                             class="relative cursor-pointer rounded-md bg-white font-medium text-blue-600 focus-within:outline-none focus-within:ring-2 focus-within:ring-blue-500 focus-within:ring-offset-2 hover:text-blue-500">
                        <span>Charger un fichier</span>
                        <input id="file-upload" name="file-upload" type="file"
                               accept=".png,.jpg,.jpeg,.pdf"
                               class="sr-only">
                      </label>
                      <p class="pl-1">ou glisser-déposer</p>
                    </div>
                    <p class="text-xs text-gray-500">PNG, JPG, JPEG, PDF</p>
                  </div>
                </div>
              </div>
              <div class="col-span-6" id="attachment" hidden>
                <dt class="text-sm font-medium ">Fichier</dt>
                <dd class="mt-1 text-sm text-gray-900">
                  <ul role="list" class="divide-y divide-gray-200 rounded-md border border-gray-200">
                    <li class="flex items-center justify-between py-3 pl-3 pr-4 text-sm">
                      <div class="flex w-0 flex-1 items-center">
                        <svg class="h-5 w-5 flex-shrink-0 text-gray-400"
                             xmlns="http://www.w3.org/2000/svg"
                             viewBox="0 0 20 20" fill="currentColor" aria-hidden="true"
                        >
                          <path fill-rule="evenodd" clip-rule="evenodd"
                                d="M15.621 4.379a3 3 0 00-4.242 0l-7 7a3 3 0 004.241 4.243h.001l.497-.5a.75.75 0 011.064 1.057l-.498.501-.002.002a4.5 4.5 0 01-6.364-6.364l7-7a4.5 4.5 0 016.368 6.36l-3.455 3.553A2.625 2.625 0 119.52 9.52l3.45-3.451a.75.75 0 111.061 1.06l-3.45 3.451a1.125 1.125 0 001.587 1.595l3.454-3.553a3 3 0 000-4.242z"
                          />
                        </svg>
                        <span class="ml-2 w-0 flex-1 truncate" id="filename">
                        resume_back_end_developer.pdf
                      </span>
                      </div>
                      <div class="ml-4 flex-shrink-0">
                        <a href="#" id="file-delete"
                           class="font-medium text-blue-600 hover:text-blue-500"
                        >Supprimer</a>
                      </div>
                    </li>
                  </ul>
                </dd>
              </div>
            </div>
          </div>
        </div>

        <script>
          document.getElementById('file-upload').addEventListener('change', function () {
            document.getElementById('attachment').hidden = false
            document.getElementById('filename').innerHTML = this.value.split('\\').pop()
            document.getElementById('uploader').hidden = true
            document.getElementById('file-delete').addEventListener('click', function () {
              document.getElementById('attachment').hidden = true
              document.getElementById('file-upload').value = ''
              document.getElementById('uploader').hidden = false
            })
          })
        </script>

        <div class="flex justify-end">
          <button type="submit"
                  class="ml-3 inline-flex justify-center rounded-md border border-transparent bg-blue-600 py-2 px-4 text-sm font-medium text-white shadow-sm hover:bg-blue-700 focus:outline-none focus:ring-2 focus:ring-blue-500 focus:ring-offset-2"
          >Créer
          </button>
        </div>
      </form>

    </div>
  </div>

@stop

@extends('layouts.default')
@section('content')
  <div class="py-6">
    <div class="mx-auto max-w-3xl sm:px-6 lg:grid lg:max-w-7xl lg:grid-cols-12 lg:gap-8 lg:px-8">
      <main class="lg:col-span-9 xl:col-span-6">
        Activités
      </main>
      <aside class="hidden xl:col-span-4 xl:block">
        <div class="sticky top-6 space-y-4">
          Informations

          <div class="bg-white px-4 py-5 sm:px-6">
            <div class="flex space-x-3">
              <div class="flex-shrink-0">
                <img class="h-10 w-10 rounded-full"
                     src="https://images.unsplash.com/photo-1550525811-e5869dd03032?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=2&w=256&h=256&q=80"
                     alt="">
              </div>
              <div class="min-w-0 flex-1">
                <p class="text-sm font-medium text-gray-900">
                  <a href="#" class="hover:underline">Fake Customer</a>
                </p>
                <p class="text-sm text-gray-500">
                  <a href="#" class="hover:underline">customer@bluesquare.io</a>
                </p>
              </div>
              <div class="flex flex-shrink-0 self-center">
                <div class="relative inline-block text-left">
                  <div>
                    <button type="button"
                            class="-m-2 flex items-center rounded-full p-2 text-gray-400 hover:text-gray-600"
                            id="menu-0-button"
                            aria-expanded="false"
                            aria-haspopup="true">
                      <span class="sr-only">Open options</span>
                      <!-- Heroicon name: mini/ellipsis-vertical -->
                      <svg class="h-5 w-5"
                           xmlns="http://www.w3.org/2000/svg"
                           viewBox="0 0 20 20"
                           fill="currentColor"
                           aria-hidden="true">
                        <path d="M10 3a1.5 1.5 0 110 3 1.5 1.5 0 010-3zM10 8.5a1.5 1.5 0 110 3 1.5 1.5 0 010-3zM11.5 15.5a1.5 1.5 0 10-3 0 1.5 1.5 0 003 0z" />
                      </svg>
                    </button>
                  </div>
                </div>
              </div>
            </div>

            <hr class="border-gray-200 my-4">

            <div class="min-w-0 flex-1">
              <p class="text-sm font-medium text-gray-900">Description</p>
              <p class="text-sm text-gray-500">{{ $ticket->description }}</p>
            </div>

            <hr class="border-gray-200 my-4">

            <div class="min-w-0 flex-1">
              <p class="text-sm font-medium text-gray-900">Page concernée</p>
              <p class="text-sm text-gray-500">{{ $ticket->url }}</p>
            </div>

            <hr class="border-gray-200 my-4">

            <div class="min-w-0 flex-1">
              <p class="text-sm font-medium text-gray-900">Statut</p>
              <p class="text-sm text-gray-500">{{ $ticket->status }}</p>
            </div>

            <hr class="border-gray-200 my-4">

            <div class="min-w-0 flex-1">
              <p class="text-sm font-medium text-gray-900">Priorité</p>
              <p class="text-sm text-gray-500">{{ $ticket->priority }}</p>
            </div>

            <hr class="border-gray-200 my-4">

            @if($ticket->files->count() > 0)
              <div class="sm:col-span-2">
                <dt class="text-sm font-medium text-indigo-600">Pièces jointes</dt>
                <dd class="mt-1 text-sm text-gray-900">
                  <ul role="list" class="divide-y divide-gray-200 rounded-md border border-gray-200">
                    @foreach($ticket->files as $file)
                      <li class="pl-3 pr-4 py-3 flex items-center justify-between text-sm">
                        <div class="w-0 flex-1 flex items-center">
                          <svg class="flex-shrink-0 h-5 w-5 text-gray-400"
                               xmlns="http://www.w3.org/2000/svg"
                               viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                            <path fill-rule="evenodd"
                                  d="M5 2a1 1 0 00-1 1v12a1 1 0 001 1h10a1 1 0 001-1V7a1 1 0 00-.293-.707l-4-4A1 1 0 0012 2H5zm5 2v5h5l-5-5z"
                                  clip-rule="evenodd" />
                          </svg>
                          <span class="ml-2 flex-1 w-0 truncate">{{ $file->name }}</span>
                        </div>
                        <div class="ml-4 flex-shrink-0">
                          <a href="{{ $file->url }}"
                             class="font-medium text-indigo-600 hover:text-indigo-500">
                            Télécharger
                          </a>
                        </div>
                        <div class="ml-4 flex-shrink-0">
                          <a href="#"
                             id="file-delete"
                             class="font-medium text-indigo-600 hover:text-indigo-500">
                            Supprimer
                          </a>
                        </div>
                      </li>
                    @endforeach
                  </ul>
                </dd>
              </div>
            @endif

          </div>
      </aside>
    </div>
  </div>
@stop

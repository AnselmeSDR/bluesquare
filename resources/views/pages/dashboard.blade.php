@extends('layouts.default')
@section('content')
  <div class="px-4 sm:px-6 lg:px-8">
    <div class="sm:flex sm:items-center">
      <div class="sm:flex-auto">
        <h1 class="text-xl font-semibold text-gray-900">Tickets</h1>
      </div>
      <div class="mt-4 sm:mt-0 sm:ml-16 sm:flex-none bg-white">
        <a href="{{ route('ticket.create') }}">
          <button type="button" class="inline-flex items-center justify-center rounded-md border border-transparent
        bg-indigo-600 px-4 py-2 text-sm font-medium text-white shadow-sm hover:bg-indigo-700 focus:outline-none
        focus:ring-2 focus:ring-indigo-500 focus:ring-offset-2 sm:w-auto">Nouveau ticket
          </button>
        </a>
      </div>
    </div>
    <div class="mt-8 flex flex-col">
      <div class="-my-2 -mx-4 overflow-x-auto sm:-mx-6 lg:-mx-8">
        <div class="inline-block min-w-full py-2 align-middle md:px-6 lg:px-8">
          <div class="overflow-hidden shadow ring-1 ring-black ring-opacity-5 md:rounded-lg">
            <table class="min-w-full divide-y divide-gray-300">
              <thead class="bg-gray-50">
              <tr>
                <th scope="col" class="py-3.5 pl-4 pr-3 text-left text-sm font-semibold text-gray-900 sm:pl-6">#</th>
                <th scope="col" class="px-3 py-3.5 text-left text-sm font-semibold text-gray-900">Titre</th>
                <th scope="col" class="px-3 py-3.5 text-left text-sm font-semibold text-gray-900">Type</th>
                <th scope="col" class="px-3 py-3.5 text-left text-sm font-semibold text-gray-900">Status</th>
                <th scope="col" class="px-3 py-3.5 text-left text-sm font-semibold text-gray-900">Priorité</th>
                <th scope="col" class="px-3 py-3.5 text-left text-sm font-semibold text-gray-900">Créateur</th>
              </tr>
              </thead>
              <tbody class="divide-y divide-gray-200 bg-white">
              @foreach($tickets as $ticket)
                <tr>


                  <td class="whitespace-nowrap px-3 py-4 text-sm text-gray-500">
                    <a href="{{ route('ticket.show', ['id'=> $ticket->id])}}">
                      {{ $ticket->id }}
                    </a>
                  </td>
                  <td class="whitespace-nowrap px-3 py-4 text-sm text-gray-500">{{ $ticket->title }}</td>

                  <td class="whitespace-nowrap px-3 py-4 text-sm text-gray-500">{{ $ticket->type }}</td>

                  <td class="whitespace-nowrap px-3 py-4 text-sm text-gray-500">
                    <span class="inline-flex rounded-full bg-green-100 px-2 text-xs font-semibold leading-5
                    text-green-800">Status</span>
                  </td>

                  <td class="whitespace-nowrap px-3 py-4 text-sm text-gray-500">{{ $ticket->priority }}</td>

                  <td class="whitespace-nowrap py-4 pl-4 pr-3 text-sm sm:pl-6">
                    <div class="flex items-center">
                      <div class="h-10 w-10 flex-shrink-0">
                        <img class="h-10 w-10 rounded-full"
                             src="{{ $ticket->user->avatar }}"
                             alt="avatar">
                      </div>
                      <div class="ml-4">
                        <div class="font-medium text-gray-900">
                          {{ $ticket->user->firstname.' '.$ticket->user->lastname }}</div>
                        <div class="text-gray-500">{{ $ticket->user->email }}</div>
                      </div>
                    </div>
                  </td>
                </tr>
              @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>

@stop

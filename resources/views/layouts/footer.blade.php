<footer class="mx-auto mt-5 w-full max-w-container px-4 sm:px-6 lg:px-8">
    <div class="border-t border-slate-900/5 py-2 border-gray-300">
        <p class="mt-5 text-center text-sm leading-6 text-slate-500">
            © {{ now()->format('Y') }} - ASDR - Bluesquare. All rights reserved.</p>
    </div>
</footer>

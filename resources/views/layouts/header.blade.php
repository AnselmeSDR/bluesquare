<div class="min-h-full">
  <header class="bg-white shadow-sm lg:static lg:overflow-y-visible">
    <div class="mx-auto max-w-7xl px-4 sm:px-6 lg:px-8">
      <div class="relative flex justify-between lg:gap-8">
        <div class="flex md:absolute md:inset-y-0 md:left-0 lg:static">
          <div class="flex flex-shrink-0 items-center">
            <img class="h-8 w-auto" src="https://tailwindui.com/img/logos/mark.svg?color=blue&shade=500"
                 alt="Your Company">
            <a href="{{ route('dashboard') }}" class="text-2xl font-bold ml-2">BLUESQUARE</a>
          </div>
        </div>


        <div class="min-w-0 flex-1 md:px-8 lg:px-0">
          <div class="flex items-center px-6 py-4 md:mx-auto md:max-w-3xl lg:mx-0 lg:max-w-none">
            <div class="w-full">
              <label for="search" class="sr-only">Search</label>
              <div class="relative">
                <div class="pointer-events-none absolute inset-y-0 left-0 flex items-center pl-3">
                  <!-- Heroicon name: mini/magnifying-glass -->
                  <svg class="h-5 w-5 text-gray-400"
                       xmlns="http://www.w3.org/2000/svg"
                       viewBox="0 0 20 20"
                       fill="currentColor"
                       aria-hidden="true">
                    <path fill-rule="evenodd"
                          d="M9 3.5a5.5 5.5 0 100 11 5.5 5.5 0 000-11zM2 9a7 7 0 1112.452 4.391l3.328 3.329a.75.75 0 11-1.06 1.06l-3.329-3.328A7 7 0 012 9z"
                          clip-rule="evenodd" />
                  </svg>
                </div>
                <input id="search"
                       name="search"
                       class="block w-full rounded-md border border-gray-300 bg-white py-2 pl-10 pr-3 text-sm placeholder-gray-500 focus:border-blue-500 focus:text-gray-900 focus:placeholder-gray-400 focus:outline-none focus:ring-1 focus:ring-blue-500 sm:text-sm"
                       placeholder="Search"
                       type="search">
              </div>
            </div>
          </div>
        </div>


        <div class="flex items-center md:absolute md:inset-y-0 md:right-0 lg:hidden">
          <!-- Mobile menu button -->
          <button type="button"
                  class="-mx-2 inline-flex items-center justify-center rounded-md p-2 text-gray-400 hover:bg-gray-100 hover:text-gray-500 focus:outline-none focus:ring-2 focus:ring-inset focus:ring-blue-500"
                  aria-expanded="false">
            <span class="sr-only">Open menu</span>

            <svg class="block h-6 w-6"
                 xmlns="http://www.w3.org/2000/svg"
                 fill="none"
                 viewBox="0 0 24 24"
                 stroke-width="1.5"
                 stroke="currentColor"
                 aria-hidden="true">
              <path stroke-linecap="round" stroke-linejoin="round" d="M3.75 6.75h16.5M3.75 12h16.5m-16.5 5.25h16.5" />
            </svg>

            <svg class="hidden h-6 w-6"
                 xmlns="http://www.w3.org/2000/svg"
                 fill="none"
                 viewBox="0 0 24 24"
                 stroke-width="1.5"
                 stroke="currentColor"
                 aria-hidden="true">
              <path stroke-linecap="round" stroke-linejoin="round" d="M6 18L18 6M6 6l12 12" />
            </svg>
          </button>
        </div>
        <div class="hidden lg:flex lg:items-center lg:justify-end">
          <a href="#"
             class="ml-6 flex-shrink-0 rounded-full bg-white p-1 text-gray-400 hover:text-gray-500 focus:outline-none
              focus:ring-2 focus:ring-blue-500 focus:ring-offset-2">
            <svg class="h-6 w-6"
                 xmlns="http://www.w3.org/2000/svg"
                 fill="none"
                 viewBox="0 0 24 24"
                 stroke-width="1.5"
                 stroke="currentColor"
                 aria-hidden="true">
              <path stroke-linecap="round"
                    stroke-linejoin="round"
                    d="M14.857 17.082a23.848 23.848 0 005.454-1.31A8.967 8.967 0 0118 9.75v-.7V9A6 6 0 006 9v.75a8.967 8.967 0 01-2.312 6.022c1.733.64 3.56 1.085 5.455 1.31m5.714 0a24.255 24.255 0 01-5.714 0m5.714 0a3 3 0 11-5.714 0" />
            </svg>
          </a>

          <div>
            <button type="button"
                    class="ml-6 flex rounded-full bg-white focus:outline-none focus:ring-2 focus:ring-blue-500
                    focus:ring-offset-2"
                    id="user-menu-button"
                    aria-expanded="false"
                    aria-haspopup="true">
              <img class="h-8 w-8 rounded-full"
                   src="{{ Auth::user()->avatar }}"
                   alt="avatar">
            </button>
          </div>

          <div class="ml-4 flex items-center md:ml-6 bg">
            <a href="#" onclick="document.getElementById('logout-form').submit()">
              <form action="{{ route('logout') }}" method="post" id="logout-form">
                @csrf
                <button type="button"
                        class="inline-flex items-center rounded-full border border-transparent p-1 shadow-sm focus:outline-none focus:ring-2 focus:ring-offset-2">
                  <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="currentColor" class="w-6 h-6">
                    <path fill-rule="evenodd"
                          d="M7.5 3.75A1.5 1.5 0 006 5.25v13.5a1.5 1.5 0 001.5 1.5h6a1.5 1.5 0 001.5-1.5V15a.75.75 0 011.5 0v3.75a3 3 0 01-3 3h-6a3 3 0 01-3-3V5.25a3 3 0 013-3h6a3 3 0 013 3V9A.75.75 0 0115 9V5.25a1.5 1.5 0 00-1.5-1.5h-6zm5.03 4.72a.75.75 0 010 1.06l-1.72 1.72h10.94a.75.75 0 010 1.5H10.81l1.72 1.72a.75.75 0 11-1.06 1.06l-3-3a.75.75 0 010-1.06l3-3a.75.75 0 011.06 0z"
                          clip-rule="evenodd" />
                  </svg>
                </button>
              </form>
            </a>
          </div>

        </div>
      </div>

      <hr class="border-gray-200">

      <nav class="hidden lg:flex lg:space-x-8 lg:py-2" aria-label="Global">
        <!-- Current: "bg-gray-100 text-gray-900", Default: "text-gray-900 hover:bg-gray-50 hover:text-gray-900" -->
        <a href="{{ route('dashboard') }}"
           class="text-gray-900 rounded-md py-2 px-3 inline-flex items-center text-sm font-medium"
           aria-current="page">
          Dashboard
        </a>

        <a href="{{ route('ticket.create') }}"
           class="text-gray-900 hover:bg-blue-50 hover:text-gray-900 rounded-md py-2 px-3 inline-flex items-center
           text-sm font-medium">
          Nouveau ticket
        </a>

        <a href="#"
           class="text-gray-900 hover:bg-blue-50 hover:text-gray-900 rounded-md py-2 px-3 inline-flex items-center
           text-sm font-medium">
          Team
        </a>

        <a href="#"
           class="text-gray-900 hover:bg-blue-50 hover:text-gray-900 rounded-md py-2 px-3 inline-flex items-center
           text-sm font-medium">
          Calendar
        </a>
      </nav>


    </div>
  </header>
</div>


<script>
  // get url to determine which menu item is active
  const url = window.location.href
  const menuItems = document.querySelectorAll('.lg\\:py-2 a')
  menuItems.forEach(item => {
    if (item.href === url) {
      item.classList.add('bg-blue-100')
    }
  })

</script>

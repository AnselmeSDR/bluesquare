<!DOCTYPE html>
<html lang="en">
  <head>
    <title>{{ env('APP_NAME') }}</title>
  </head>
  <script src="https://cdn.tailwindcss.com"></script>
  <body class="bg-gray-50">

    @auth()
      <div class="flex flex-1 flex-col">
        @include('layouts.header')
        <main>
          <div class="mx-auto px-4 sm:px-6 md:px-8">
            <div class="py-5">
              @yield('content')
            </div>
            @include('layouts.footer')
          </div>
        </main>
      </div>
    @else
      <div class="flex flex-1 flex-col">
        <main>
          <div class="py-6">
            <div class="mx-auto max-w-7xl px-4 sm:px-6 md:px-8">
              @yield('content')
              @include('layouts.footer')
            </div>
          </div>
        </main>
      </div>
    @endauth
  </body>
</html>

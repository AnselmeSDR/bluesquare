<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400" alt="Laravel Logo"></a></p>

<p align="center">
<a href="https://github.com/laravel/framework/actions"><img src="https://github.com/laravel/framework/workflows/tests/badge.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/dt/laravel/framework" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/v/laravel/framework" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/l/laravel/framework" alt="License"></a>
</p>

## About Project

Test technique pour une entreprise de developpement web.  
Réalisation d'une application de ticketing pour une entreprise de developpement web.  
Le temps imparti est de 2h30 et libre du choix de techno.  

Les pages à réaliser sont les suivantes :
- Datatable des tickets
- Formulaire de création de ticket
- Affichage d'un ticket


Techno utilisé :
- PHP 8.2
- Laravel 9.48
- Postgres 15
- Sail 8.2

## Install Project

- Clone project
- ./vendor/bin/sail up
- ./vendor/bin/sail artisan migrate --seed
- ./vendor/bin/sail artisan storage:link
- Go to http://localhost:8080
- anselme8@icloud.com - password

## License

The Laravel framework is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).

<?php

namespace App\Models;

use App\Models\File;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Validator;

class Ticket extends Model {
  use HasFactory, SoftDeletes;

  protected $table = 'tickets';
  protected $fillable = [
      'type',
      'title',
      'description',
      'status',
      'priority',
      'url',
      'user_id'
  ];

  /**
   * Validator for Create model
   *
   * @param $data
   *
   * @return \Illuminate\Validation\Validator
   */
  public static function validateCreate($data) {
    return Validator::make($data, [
        'type'        => 'required|max:255|string',
        'title'       => 'required|max:255|string',
        'description' => 'required|max:255|string',
        'status'      => 'required|max:255|string',
        'priority'    => 'required|max:255|string',
        'url'         => 'required|max:255|string',
        'user_id'     => 'required|integer|exists:App\Models\User,id'
    ]);
  }

  /**
   * Validator for Update model
   *
   * @param $data
   * @param $id
   *
   * @return \Illuminate\Validation\Validator
   */
  public static function validateUpdate($data, $id) {
    return Validator::make($data, [
        'type'        => 'max:255|string',
        'title'       => 'max:255|string',
        'description' => 'max:255|string',
        'status'      => 'max:255|string',
        'priority'    => 'max:255|string',
        'url'         => 'max:255|string',
        'user_id'     => 'integer|exists:App\Models\User,id'
    ]);
  }

  public function user() {
    return $this->belongsTo(User::class);
  }

  public function files() {
    return $this->morphMany(File::class, 'fileable');
  }

}

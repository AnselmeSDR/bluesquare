<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\MorphTo;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class File extends Model {
  use HasFactory, SoftDeletes;

  /**
   * @var string $table   Table's name in DB
   * @var array $fillable Data that can be modified by the user
   */
  protected $table = 'files';
  protected $fillable = ['name', 'path'];
  protected $appends = ['url'];

  /**
   * Validator for Create model
   *
   * @param $data
   *
   * @return \Illuminate\Contracts\Validation\Validator
   */
  public static function validateCreate($data) {
    return Validator::make($data, [
        'name' => 'required|max:255',
        'path' => 'required|max:255'
    ]);
  }


  /**
   * Validator for Update model
   *
   * @param $data
   *
   * @return \Illuminate\Contracts\Validation\Validator
   */
  public static function validateUpdate($data) {
    return Validator::make($data, [
        'name' => 'required|max:255',
        'path' => 'required|max:255'
    ]);
  }

  protected static function boot() {
    parent::boot();
    static::deleting(function ($file) {
      Storage::delete($file->path);
    });
  }

  /**
   * @return MorphTo
   */
  public function fileable() : MorphTo {
    return $this->morphTo();
  }

  public function getUrlAttribute() {
    return Storage::url($this->path);
  }
}

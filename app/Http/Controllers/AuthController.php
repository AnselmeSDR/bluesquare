<?php
/*
 * Copyright (c) 2022. Le16com. All rights reserved.
 *
 * Author: Anselme SCHNEIDER <anselme8@icloud.com>
 * Contributor: Anselme SCHNEIDER <anselme@le16com.com>
 */

namespace App\Http\Controllers;

use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

/**
 * Class AuthController
 */
class AuthController extends Controller {


  public function login() {
    if (auth()->check()) {
      return redirect()->route('/');
    }
    return view('auth.login');
  }

  public function authenticate(Request $request) : RedirectResponse {
    $request->validate([
        'email'    => 'required|email',
        'password' => 'required',
    ]);

    if (auth()->attempt(['email' => request('email'), 'password' => request('password')])) {
      $request->session()->regenerate();

      return redirect()->intended();
    }

    return back()->withErrors([
        'email' => 'The provided credentials do not match our records.',
    ]);
  }

  public function logout() {
    auth()->logout();

    return redirect()->route('login');
  }
}

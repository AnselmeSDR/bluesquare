<?php

namespace App\Http\Controllers;

use App\Models\Ticket;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

class TicketController extends Controller {

  public function create() {
    return view('pages.ticket.ticket-create');
  }

  public function store(Request $request) {
    $data = $request->all();
    $data['user_id'] = auth()->user()->id;
    $data['status'] = 'open';
    $validator = Ticket::validateCreate($data);
    if ($validator->fails()) {
      throw new ValidationException($validator, $validator->errors());
    }

    $ticket = Ticket::create($data);

    if ($request->hasFile('file-upload')) {
      $file = $request->file('file-upload');
      $fileFilename = preg_replace('/\s+/', '_', trim($file->getClientOriginalName()));
      $ticket->files()->create([
          'name' => $fileFilename,
          'path' => $file->storeAs($ticket->id, $fileFilename, 'public')
      ]);
    }

    return redirect()->route('ticket.show', $ticket);
  }

  public function show($id) {
    $ticket = Ticket::findOrFail($id);
    return view('pages.ticket.ticket-show', compact('ticket'));
  }
}

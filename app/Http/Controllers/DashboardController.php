<?php
/*
 * Copyright (c) 2022. Le16com. All rights reserved.
 *
 * Author: Anselme SCHNEIDER <anselme8@icloud.com>
 * Contributor: Anselme SCHNEIDER <anselme@le16com.com>
 */

namespace App\Http\Controllers;

use App\Models\Ticket;

/**
 * Class DashboardController
 */
class DashboardController extends Controller {

  public function __invoke() {
    $tickets = Ticket::all();
    $tickets->load('user', 'files');

    return view('pages.dashboard', compact('tickets'));
  }
}

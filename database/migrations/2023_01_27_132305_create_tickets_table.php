<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up() {
    Schema::disableForeignKeyConstraints();

    Schema::create('tickets', function (Blueprint $table) {
      $table->engine = 'InnoDB';
      $table->charset = 'utf8';
      $table->collation = 'utf8_general_ci';

        $table->id();
        $table->string('type');
        $table->string('title');
        $table->text('description');
        $table->string('status')->default('open');
        $table->string('priority');
        $table->string('url');

        $table->timestamps();
        $table->softDeletes();
        $table->foreignId('user_id')->constrained()->cascadeOnDelete();
    });

    Schema::enableForeignKeyConstraints();
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down() {
    Schema::dropIfExists('tickets');
  }
};

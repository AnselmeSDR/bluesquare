<?php
/*
 * Copyright (c) 2022. Le16com. All rights reserved.
 *
 * Author: Anselme SCHNEIDER <anselme8@icloud.com>
 * Contributor: Anselme SCHNEIDER <anselme@le16com.com>
 */

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up() {
    Schema::disableForeignKeyConstraints();

    Schema::create('users', function (Blueprint $table) {
      $table->engine = 'InnoDB';
      $table->charset = 'utf8';
      $table->collation = 'utf8_general_ci';

      $table->id();
      $table->string('firstname');
      $table->string('lastname');
      $table->string('email')->unique();
      $table->string('password');
      $table->string('avatar');

      $table->timestamps();
      $table->softDeletes();
    });

    Schema::enableForeignKeyConstraints();
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down() {
    Schema::dropIfExists('users');
  }
};

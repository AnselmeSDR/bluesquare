<?php

namespace Database\Factories;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Ticket>
 */
class TicketFactory extends Factory
{
  /**
   * Define the model's default state.
   *
   * @return array<string, mixed>
   * @throws \Exception
   */
    public function definition()
    {
        return [
            'type' => $this->faker->word,
            'title' => $this->faker->sentence,
            'description' => $this->faker->paragraph,
            'status' => $this->faker->word,
            'priority' => $this->faker->word,
            'url' => $this->faker->url,
            'user_id' => User::all()->random()->id
        ];
    }
}

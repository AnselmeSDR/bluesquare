<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Facades\Http;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\User>
 */
class UserFactory extends Factory {
  /**
   * Define the model's default state.
   *
   * @return array<string, mixed>
   */
  public function definition() {
    $faker = \Faker\Factory::create('fr_FR');

    $gender = $faker->randomElement(['male', 'female']);
    $firstname = $faker->firstName($gender);
    $lastname = strtoupper($faker->lastName());
    $email = transliterator_transliterate('Any-Latin; Latin-ASCII; Lower()', $firstname . $lastname . '@exemple.org');

    $res = Http::get('https://xsgames.co/randomusers/avatar.php?g=' . $gender);
    $avatar = $res->transferStats->getHandlerStats()['url'];

    return [
        'firstname' => $firstname,
        'lastname'  => $lastname,
        'email'     => $email,
        'password'  => bcrypt('password'),
        'avatar'    => $avatar,
    ];
  }
}

<?php

namespace Database\Seeders;

use App\Models\Ticket;
use Illuminate\Database\Seeder;

class TicketSeeder extends Seeder {
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run() {
    $faker = \Faker\Factory::create('fr_FR');

    Ticket::factory()->count(10)->create();
  }
}

<?php
/*
 * Copyright (c) 2022. Le16com. All rights reserved.
 *
 * Author: Anselme SCHNEIDER <anselme8@icloud.com>
 * Contributor: Anselme SCHNEIDER <anselme@le16com.com>
 */

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;

/**
 * Class UserSeeder
 */
class UserSeeder extends Seeder {

  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run() {
    $faker = \Faker\Factory::create('fr_FR');

    User::factory()->count(20)->create();

    User::create([
        'firstname' => 'Anselme',
        'lastname'  => 'Schneider',
        'email'     => 'anselme8@icloud.com',
        'password'  => bcrypt('password'),
        'avatar'    => 'https://1.gravatar.com/avatar/2f34a7187b40c7fcce6f2af3916421d4'
    ]);
  }
}

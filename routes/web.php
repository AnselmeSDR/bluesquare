<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\TicketController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', DashboardController::class)
     ->name('dashboard')
     ->middleware(['auth']);

Route::get('login', [AuthController::class, 'login'])
     ->name('login');
Route::post('login', [AuthController::class, 'authenticate'])
     ->name('authenticate');
Route::post('logout', [AuthController::class, 'logout'])
     ->name('logout');

Route::get('ticket', [TicketController::class, 'create'])
     ->name('ticket.create')
     ->middleware(['auth']);
Route::post('ticket', [TicketController::class, 'store'])
     ->name('ticket.store')
     ->middleware(['auth']);
Route::get('ticket/{id}', [TicketController::class, 'show'])
     ->name('ticket.show')
     ->middleware(['auth']);
Route::post('ticket/{id}/update', [TicketController::class, 'update'])
     ->name('ticket.update')
     ->middleware(['auth']);
Route::post('ticket/{id}/delete', [TicketController::class, 'delete'])
     ->name('ticket.delete')
     ->middleware(['auth']);
